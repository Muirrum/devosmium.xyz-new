---
title: "New Site"
date: 2018-10-25T19:07:01-04:00
draft: false

categories: ["announcement"]
tags: []
author: "DevOsmium"
---
Hey guys, you may have noticed the new CSS and layout. This is because I have now moved away from Jekyll to Hugo, which is, (at least in my opinion), so much better than jekyll. It's on package managers, which makes it easier to install. It runs and builds a lot faster, and it looks far cleaner than the old site. I'm not going to go into much detail, but the new repo is here: https://gitlab.com/Dev-Osmium/devosmium.xyz-new